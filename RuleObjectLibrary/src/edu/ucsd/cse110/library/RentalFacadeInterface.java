package edu.ucsd.cse110.library;

public interface RentalFacadeInterface extends Library{
	public void checkoutPublication(Member member, Publication publication);
	public void returnPublication(Publication publication);
	public double getFee(Member member);
	public boolean hasFee(Member member);
}
