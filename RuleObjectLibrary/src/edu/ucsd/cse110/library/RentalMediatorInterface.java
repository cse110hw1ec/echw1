package edu.ucsd.cse110.library;

public interface RentalMediatorInterface extends RentalMediator{
	public void registerColleague(RentalColleagueInterface colleague);
	public void checkoutPublication(Member member, Publication publication);
	public void returnPublication(Publication publication);
	public double getFee(Member member);
	public boolean hasFee(Member member);

}
