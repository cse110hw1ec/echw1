package edu.ucsd.cse110.library;

public class RentalMediator extends RentalMediatorInterface{
	private RentalFacadeInteface rentalFacade;
	private ArrayList<Members> members;
	private ArrayList<RuleObject> feeRules;
	
	public void registerColleague(RentalColleagueInterface colleague){
		rentalFacade.makeComponent(member, publication);
	}
	public void checkoutPublication(Member member, Publication publication){
		members.add(member);
	}
	public void returnPublication(Publication publication){
		member.returnPublication(publication);
	}
	public double getFee(Member member){
		publication.getFee(member);
	}
	public boolean hasFee(Member member){
		publication.hasFee(member);
	}
}
