package edu.ucsd.cse110.library;

public class RentingFacadeFactory extends ComponentFactory{
	RentalFacadeInterface makeComponent(Member member, Publication publication){
		return RentalFacadeInterface(member, publication);
	}
}
