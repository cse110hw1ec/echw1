package edu.ucsd.cse110.library;

public class RentalFacade {
	private Member member;
	private Publication publication;
	private double fee;
		
	public void checkoutPublication(Member member, Publication publication){
		publication.checkout(member, date);
		
	}
	
	public void returnPublication(Publication publication){
		member.pubReturn(date);
	
	}
	public double getFee(Member member){
		return member.getDueFees();
	}
	public boolean hasFee(Member member){
		return member.applyLateFee(fee);
	}
}
