package edu.ucsd.cse110.library.rules;

public interface Facade {
	public void checkoutPublication(Member, Publication);
	public void returnPublication(Publication);
	public double getFee(Member);
	public boolean hasFee(Member);
}